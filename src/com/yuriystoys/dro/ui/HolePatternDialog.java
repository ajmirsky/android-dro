/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.data.Repository;

public class HolePatternDialog extends DialogFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_hole_pattern, null);

		setEventListeners(view);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.hole_pattern_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false);

		builder.setPositiveButton(R.string.hole_pattern_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing here. The real handler is below
			}
		});

		builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		Dialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {

				Window window = ((AlertDialog) dialog).getWindow();

				if (getResources().getConfiguration().screenHeightDp < 400)
					window.setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);

				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (createPattern()) {
									((DroApplication) getActivity().getApplication()).getCurrentWorkspace().refresh();
									dismiss();
								}
							}
						});

			}
		});

		dialog.setCanceledOnTouchOutside(false);

		return dialog;

	}

	private boolean createPattern() {

		double angleAlpha = 0;

		double xCenter = 0;
		double yCenter = 0;

		double xStep = 0;
		double yStep = 0;

		int xRepeat = 1;
		int yRepeat = 1;

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		if (_angleEdit.getEditableText().length() > 0)
			try {
				angleAlpha = Double.parseDouble(_angleEdit.getEditableText().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_pattern_alpha_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		while (angleAlpha < 0) // normalize the angle (-90 = 270, etc.)
		{
			angleAlpha = 360 - angleAlpha;
		}

		// parse repeats

		if (_xRepeatSpinner.getSelectedItemPosition() > 0)
			try {

				xRepeat = Integer.parseInt(_xRepeatSpinner.getSelectedItem().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_pattern_x_repeat_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		if (_yRepeatSpinner.getSelectedItemPosition() > 0)
			try {

				yRepeat = Integer.parseInt(_yRepeatSpinner.getSelectedItem().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_pattern_y_repeat_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		// parse steps

		if (xRepeat > 1 && _xStepEdit.getEditableText().length() > 0)
			try {
				xStep = Double.parseDouble(_xStepEdit.getEditableText().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_pattern_x_step_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		if (yRepeat > 1 && _yStepEdit.getEditableText().length() > 0)
			try {
				yStep = Double.parseDouble(_yStepEdit.getEditableText().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_pattern_y_step_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		// figure out where the center will be

		switch (_location.getSelectedItemPosition()) {
		case 0: // current
			xCenter = dro.getAxis(Axis.X).getAbsolutePosition();
			yCenter = dro.getAxis(Axis.Y).getAbsolutePosition();

			break;
		case 1: // zero
			xCenter = 0;
			yCenter = 0;

			break;

		case 2: // custom (get the values from the user)

			if (_xEditText.getEditableText().length() > 0)
				try {
					xCenter = Double.parseDouble(_xEditText.getEditableText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.hole_pattern_x_invalid, Toast.LENGTH_LONG).show();
					return false;
				}

			if (_yEditText.getEditableText().length() > 0)
				try {
					yCenter = Double.parseDouble(_yEditText.getEditableText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.hole_pattern_y_invalid, Toast.LENGTH_LONG).show();
					return false;
				}

			break;
		default:
			return false;
		}

		// do some basic sanity checks

		if (xRepeat <= 1 && yRepeat <= 1) {
			Toast.makeText(getActivity(), R.string.hole_pattern_repeat_invalid, Toast.LENGTH_LONG).show();
			return false;
		} else if (xRepeat > 1 && xStep <= 0) {
			Toast.makeText(getActivity(), R.string.hole_pattern_x_step_required, Toast.LENGTH_LONG).show();
			return false;
		} else if (yRepeat > 1 && yStep <= 0) {
			Toast.makeText(getActivity(), R.string.hole_pattern_y_step_required, Toast.LENGTH_LONG).show();
			return false;
		}

		// create the points

		List<Point> points = new ArrayList<Point>();

		// x uses SIN, y uses COS
		// formula: x = SIN(angle) * radius
		// y = COS(angle) * radius

		double xPosition;
		double yPosition;

		double xDelta;
		double yDelta;

		double xPositionActual = 0;
		double yPositionActual = 0;

		double sine = Math.sin(Math.toRadians(-angleAlpha));
		double cosine = Math.cos(Math.toRadians(-angleAlpha));

		yPosition = yCenter;

		// create points
		for (int y = 0; y < yRepeat; y++) {
			// going in y direction first because the angle is in relation to
			// the y axis

			xPosition = xCenter;

			for (int x = 0; x < xRepeat; x++) {

				xDelta = xPosition - xCenter;
				yDelta = yPosition - yCenter;

				xPositionActual = xDelta * cosine - yDelta * sine + xCenter;
				yPositionActual = xDelta * sine + yDelta * cosine + yCenter;

				Point point = Point.createPoint2D(null, null, dro.getAxis(Axis.X).convertToCounts(xPositionActual), dro
						.getAxis(Axis.Y).convertToCounts(yPositionActual), 0);

				points.add(point);

				xPosition = xPosition + xStep;

			}

			yPosition = yPosition + yStep;
		}

		Repository repo = Repository.open(getActivity());

		repo.insertPointGroup(((DroApplication) getActivity().getApplication()).getCurrentWorkspace().getId(), "Grid",
				points.toArray(new Point[xRepeat * yRepeat]));

		return true;
	}

	private Spinner _location;

	private Spinner _xRepeatSpinner;
	private Spinner _yRepeatSpinner;

	private EditText _angleEdit;

	private EditText _xStepEdit;
	private EditText _yStepEdit;

	private EditText _xEditText;
	private EditText _yEditText;

	private View _customPointView;

	private void setEventListeners(View view) {

		View temp = view.findViewById(R.id.customPointView);

		if (temp != null) {
			_customPointView = temp;
			_customPointView.setVisibility(View.INVISIBLE);
		}

		temp = view.findViewById(R.id.xStepEdit);
		if (temp != null && temp instanceof EditText) {
			_xStepEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.yStepEdit);
		if (temp != null && temp instanceof EditText) {
			_yStepEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.xRepeatSpinner);
		if (temp != null && temp instanceof Spinner) {
			_xRepeatSpinner = (Spinner) temp;

			_xRepeatSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					if (position > 0) {
						_xStepEdit.setEnabled(true);
						_xStepEdit.requestFocus();
					} else {
						_xStepEdit.setText("");
						_xStepEdit.setEnabled(false);
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					_xStepEdit.setText("");
					_xStepEdit.setEnabled(false);
				}
			});
		}

		temp = view.findViewById(R.id.yRepeatSpinner);
		if (temp != null && temp instanceof Spinner) {
			_yRepeatSpinner = (Spinner) temp;

			_yRepeatSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					if (position > 0) {
						_yStepEdit.setEnabled(true);
						_yStepEdit.requestFocus();
					} else {
						_yStepEdit.setText("");
						_yStepEdit.setEnabled(false);
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {
					_yStepEdit.setText("");
					_yStepEdit.setEnabled(false);
				}
			});
		}

		temp = view.findViewById(R.id.angleEdit);
		if (temp != null && temp instanceof EditText) {
			_angleEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.xCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_xEditText = (EditText) temp;
		}

		temp = view.findViewById(R.id.yCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_yEditText = (EditText) temp;
		}

		temp = view.findViewById(R.id.locationSpinner);
		if (temp != null && temp instanceof Spinner) {
			_location = (Spinner) temp;

			_location.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

					switch (position) {
					case 0: // current

						if (_customPointView != null) {
							_customPointView.setVisibility(View.INVISIBLE);
						}

						if (_xEditText != null) {
							_xEditText.setText(R.string.zero_thousandths);
						}

						if (_yEditText != null) {
							_yEditText.setText(R.string.zero_thousandths);
						}

						break;

					case 1: // zero

						if (_customPointView != null) {
							_customPointView.setVisibility(View.INVISIBLE);
						}

						if (_xEditText != null) {
							_xEditText.setText(R.string.zero_thousandths);
						}

						if (_yEditText != null) {
							_yEditText.setText(R.string.zero_thousandths);
						}

						break;

					case 2: // custom

						if (_customPointView != null) {
							_customPointView.setVisibility(View.VISIBLE);
						}

						break;
					default:
						break;
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});
		}

	}

	public static void Show(Activity activity) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new HolePatternDialog();
		newFragment.show(ft, "dialog");
	}
}
