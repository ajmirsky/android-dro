/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.content.DialogInterface.OnShowListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.yuriystoys.dro.DroApplication;
import com.yuriystoys.dro.R;
import com.yuriystoys.dro.axes.Axis;
import com.yuriystoys.dro.core.Dro;
import com.yuriystoys.dro.core.Point;
import com.yuriystoys.dro.data.Repository;

public class HoleCircleDialog extends DialogFragment {

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		View view = getActivity().getLayoutInflater().inflate(R.layout._fragment_hole_circle, null);

		setEventListeners(view);

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setTitle(R.string.hole_circle_title);
		builder.setCancelable(false);
		builder.setView(view);

		// set dialog message
		builder.setCancelable(false);

		builder.setPositiveButton(R.string.hole_circle_ok, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// do nothing here. The real handler is below
			}
		});

		builder.setNegativeButton(R.string.dialog_cancel, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				// if this button is clicked, just close
				// the dialog box and do nothing
				dialog.cancel();
			}
		});

		// create alert dialog
		AlertDialog dialog = builder.create();
		dialog.setCanceledOnTouchOutside(false);

		dialog.setOnShowListener(new OnShowListener() {

			@Override
			public void onShow(DialogInterface dialog) {
				
				Window window = ((AlertDialog) dialog).getWindow();
				
				if(getResources().getConfiguration().screenHeightDp<400)
					window.setLayout(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
				
				((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
						new View.OnClickListener() {
							@Override
							public void onClick(View v) {
								if (createCircle()) {
									((DroApplication) getActivity().getApplication()).getCurrentWorkspace().refresh();
									dismiss();
								}
							}
						});
		
			}
		});

		dialog.setCanceledOnTouchOutside(false);


		return dialog;

	}

	private boolean createCircle() {

		int holeCount = 0;
		double radius = 0;

		double angleAlpha = 0;
		double angleTheta = 0;
		double angleBetweenHoles = 0;

		double xCenter = 0;
		double yCenter = 0;

		Dro dro = ((DroApplication) getActivity().getApplication()).getDro();

		if (_holeCountSpinner.getSelectedItem() != null)
			holeCount = Integer.parseInt(_holeCountSpinner.getSelectedItem().toString());

		if (_radiusEdit.getEditableText().length() > 0)
			try {
				radius = Double.parseDouble(_radiusEdit.getEditableText().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_circle_radius_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		if (radius <= 0) {
			Toast.makeText(getActivity(), R.string.hole_circle_radius_zero, Toast.LENGTH_LONG).show();
			return false;
		}

		if (_angleEditAlpha.getEditableText().length() > 0)
			try {
				angleAlpha = Double.parseDouble(_angleEditAlpha.getEditableText().toString());
			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_circle_alpha_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		if (_angleEditTheta.getEditableText().length() > 0)
			try {
				angleTheta = Double.parseDouble(_angleEditTheta.getEditableText().toString());

			} catch (NumberFormatException ex) {
				Toast.makeText(getActivity(), R.string.hole_circle_theta_invalid, Toast.LENGTH_LONG).show();
				return false;
			}

		while (angleAlpha < 0) // normalize the angle (-90 = 270, etc.)
		{
			angleAlpha = 360 - angleAlpha;
		}

		if (angleTheta > 360) {
			Toast.makeText(getActivity(), R.string.hole_circle_theta_too_large, Toast.LENGTH_LONG).show();
			return false;
		}

		// figure out where the center will be

		switch (_location.getSelectedItemPosition()) {
		case 0: // current
			xCenter = dro.getAxis(Axis.X).getAbsolutePosition();
			yCenter = dro.getAxis(Axis.Y).getAbsolutePosition();

			break;
		case 1: // zero
			xCenter = 0;
			yCenter = 0;

			break;

		case 2: // custom (get the values from the user)

			if (_xEditText.getEditableText().length() > 0)
				try {
					xCenter = Double.parseDouble(_xEditText.getEditableText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.hole_circle_x_invalid, Toast.LENGTH_LONG).show();
					return false;
				}

			if (_yEditText.getEditableText().length() > 0)
				try {
					yCenter = Double.parseDouble(_yEditText.getEditableText().toString());
				} catch (NumberFormatException ex) {
					Toast.makeText(getActivity(), R.string.hole_circle_y_invalid, Toast.LENGTH_LONG).show();
					return false;
				}

			break;
		default:
			return false;
		}

		// create the points

		List<Point> points = new ArrayList<Point>();

		angleBetweenHoles = Math.toRadians(angleTheta == 0 || angleTheta == 360 ? 360 / holeCount : angleTheta / (holeCount-1));

		// x uses SIN, y uses COS
		// formula: x = SIN(angle) * radius
		// y = COS(angle) * radius

		double angle = Math.toRadians(angleAlpha);
		double xPosition = 0;
		double yPosition = 0;

		for (int i = 0; i < holeCount; i++) {

			xPosition = xCenter + Math.sin(angle) * radius;
			yPosition = yCenter + Math.cos(angle) * radius;

			Point point = Point.createPoint2D(null, null, 
					dro.getAxis(Axis.X).convertToCounts(xPosition),
					dro.getAxis(Axis.Y).convertToCounts(yPosition), 0);

			points.add(point);
			
			angle += angleBetweenHoles;
		}

		Repository repo = Repository.open(getActivity());

		repo.insertPointGroup(((DroApplication) getActivity().getApplication()).getCurrentWorkspace().getId(),
				"Circle", points.toArray(new Point[holeCount]));

		return true;
	}

	private Spinner _holeCountSpinner;
	private Spinner _location;

	private EditText _radiusEdit;
	private EditText _angleEditAlpha;
	private EditText _angleEditTheta;
	private EditText _xEditText;
	private EditText _yEditText;

	private View _customPointView;

	private void setEventListeners(View view) {

		View temp = view.findViewById(R.id.customPointView);

		if (temp != null) {
			_customPointView = temp;
			_customPointView.setVisibility(View.INVISIBLE);
		}

		temp = view.findViewById(R.id.holeCountSpinner);
		if (temp != null && temp instanceof Spinner) {
			_holeCountSpinner = (Spinner) temp;
		}

		temp = view.findViewById(R.id.radiusEdit);
		if (temp != null && temp instanceof EditText) {
			_radiusEdit = (EditText) temp;
		}

		temp = view.findViewById(R.id.angleAlphaEdit);
		if (temp != null && temp instanceof EditText) {
			_angleEditAlpha = (EditText) temp;
		}

		temp = view.findViewById(R.id.angleThetaEdit);
		if (temp != null && temp instanceof EditText) {
			_angleEditTheta = (EditText) temp;
		}

		temp = view.findViewById(R.id.xCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_xEditText = (EditText) temp;
		}

		temp = view.findViewById(R.id.yCoordEdit);
		if (temp != null && temp instanceof EditText) {
			_yEditText = (EditText) temp;
		}

		temp = view.findViewById(R.id.locationSpinner);
		if (temp != null && temp instanceof Spinner) {
			_location = (Spinner) temp;

			_location.setOnItemSelectedListener(new OnItemSelectedListener() {

				@Override
				public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

					switch (position) {
					case 0: // current

						if (_customPointView != null) {
							_customPointView.setVisibility(View.INVISIBLE);
						}

						if (_xEditText != null) {
							_xEditText.setText(R.string.zero_thousandths);
						}

						if (_yEditText != null) {
							_yEditText.setText(R.string.zero_thousandths);
						}

						break;

					case 1: // zero

						if (_customPointView != null) {
							_customPointView.setVisibility(View.INVISIBLE);
						}

						if (_xEditText != null) {
							_xEditText.setText(R.string.zero_thousandths);
						}

						if (_yEditText != null) {
							_yEditText.setText(R.string.zero_thousandths);
						}

						break;

					case 2: // custom

						if (_customPointView != null) {
							_customPointView.setVisibility(View.VISIBLE);
						}

						break;
					default:
						break;
					}
				}

				@Override
				public void onNothingSelected(AdapterView<?> arg0) {

				}
			});
		}

	}

	public static void Show(Activity activity) {

		FragmentTransaction ft = activity.getFragmentManager().beginTransaction();
		Fragment prev = activity.getFragmentManager().findFragmentByTag("dialog");
		if (prev != null) {
			ft.remove(prev);
		}
		ft.addToBackStack(null);

		// Create and show the dialog.
		DialogFragment newFragment = new HoleCircleDialog();
		newFragment.show(ft, "dialog");
	}
}
