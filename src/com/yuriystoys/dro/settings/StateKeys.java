/* Copyright 2012-2013 Yuriy Krushelnytskiy.
 *
 * This file is part of YuriysToys.com Touch DRO
 *
 * Touch DRO is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */

package com.yuriystoys.dro.settings;

public final class StateKeys {

	private StateKeys() {}
	
	public static final String STATE_SELECTED_NAVIGATION_ITEM = "selected_navigation_item";
	

	public final static String X_RELATIVE = "x_relative";
	public final static String Y_RELATIVE = "y_relative";
	public final static String Z_RELATIVE = "z_relative";
	
	public final static String X_OFFSET = "x_offset";
	public final static String Y_OFFSET = "y_offset";
	public final static String Z_OFFSET = "z_offset";
	
	public final static String SELECTED_WORKSPACE = "selected_workspace";
	public final static String SELECTED_POINT = "selected_point";
	
	public final static String LAST_CONNECTION_CONNECTED = "last_connection_connected";
	public final static String LAST_CONNECTION_USB = "last_connection_usb";
	public final static String LAST_CONNECTION_BT_DEVICE = "last_connection_bt_device";
}
